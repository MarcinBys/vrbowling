﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    private Toggle toggleSwitch;


    void Awake()        //Awake jest wczytywane przed wszystkimi innymi metodami (Start, Update itd.)
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
    }

    private void Start()
    {
        if (GetCurrentSceneName().Equals("Menu"))
        {
            toggleSwitch = GameObject.FindGameObjectWithTag("VRController").GetComponent<Toggle>();
            toggleSwitch.isOn = Convert.ToBoolean(PlayerPrefs.GetInt("VR_On", Convert.ToInt16(toggleSwitch.isOn)));
        }
    }


    /// <summary>
    /// Method used to check if name of loaded scene contains given string.
    /// </summary>
    /// <param name="scene"> Fragment of scene name. </param>
    /// <returns> Returns true if scene name contains given string. </returns>
    public bool CheckSceneName(string scene)
    {
        if (SceneManager.GetActiveScene().name.Contains(scene))
        {
            return true;
        }

        return false;
    }


    /// <summary>
    /// Function returning current scene name.
    /// </summary>
    /// <returns>Returns current scene name.</returns>
    public string GetCurrentSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }


    /// <summary>
    /// Loads scene by its name.
    /// </summary>
    /// <param name="name">Scene name</param>
    public void LoadLevel(string name)
    {
        Debug.Log("Level load requested for: " + name);
        SceneManager.LoadScene(name);
    }


    /// <summary>
    /// Request quitting the game.
    /// </summary>
    public void QuitRequest()
    {
        Debug.Log("Quit requested.");
        Application.Quit();
    }

    public void VRControl()
    {
        PlayerPrefs.SetInt("VR_On", Convert.ToInt16(toggleSwitch.isOn));
        Debug.Log(PlayerPrefs.GetInt("VR_On"));
        /*
        if (toggleSwitch.isOn)
        {
            PlayerPrefs.SetInt("VR_On", 1);
        }
        else if (!toggleSwitch.isOn)
        {
            PlayerPrefs.SetInt("VR_On", 0);
            Debug.Log(PlayerPrefs.GetInt("VR_On"));
        }*/
    }
}