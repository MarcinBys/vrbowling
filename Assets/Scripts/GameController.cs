﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    // <<< Controlling falling out of level >>>
    public float minimalHeight;
    private Vector3 startPosition;

    public AudioClip[] audioClip;
    public AudioSource audioSource;

    // <<< GUI >>>
    private int score;
    public Text scoreText;
    public Text timerText;
    public Text endText;
    public Text highScoreText;


    // <<< Other scripts >>>
    private PlayerController player;


    // <<< TIMER >>>
    public bool isTimer = true;
    public float timeLeft = 0;


    // <<< LEVEL CONTROL >>>
    private bool finished = false;



    void Start()
    {
        //pc = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
        player = FindObjectOfType<PlayerController>();
        startPosition = player.transform.position;

        score = 0;
        UpdateScore();
        UpdateHighScore();

        endText.text = "";
    }


    // Update is called once per frame
    void Update()
    {
        CheckIfFallOut();

        if (!finished)
        {
            Timer();
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            LevelManager.Instance.LoadLevel("Menu");
        }

        if (Input.GetKey(KeyCode.F5))
        {
            LevelManager.Instance.LoadLevel(LevelManager.Instance.GetCurrentSceneName());
        }
    }


    /// <summary>
    /// Add points to overall score (when the current level is not finished).
    /// </summary>
    /// <param name="points">How many points you want award player.</param>
    public void AddScore(int points)
    {
        if (!finished)
        {
            if (isTimer && timeLeft >= 0) // Multiply points by time left (if there is a timer on the level)
            {
                score += Mathf.RoundToInt(points * timeLeft);
            }
            else
            {
                score += points;
            }
        }

        UpdateScore();
    }


    /// <summary>
    /// Update GUI score text
    /// </summary>
    private void UpdateScore()
    {
        scoreText.text = "Score: " + score.ToString();
    }


    /// <summary>
    /// Function used to control if player fell out of level
    /// </summary>
    private void CheckIfFallOut()
    {
        if (player.transform.position.y < minimalHeight)
        {
            player.transform.position = startPosition;
            player.rb.velocity = new Vector3(0, 0, 0);
            player.rb.angularVelocity = new Vector3(0, 0, 0);
        }
    }


    /// <summary>
    /// Function starts and controls in-game timer.
    /// If isTimer is true, it starts the countdown from timeLeft to 0.
    /// </summary>
    private void Timer()
    {
        if (!isTimer)
        {
            timerText.text = "";
        }
        else if (isTimer && timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            timerText.text = timeLeft.ToString("#");
        }
        else if (isTimer && timeLeft <= 0)
        {
            timerText.text = "0";
            TheEnd();
        }
    }



    public void AddTime(int value)
    {
        timeLeft += value;
    }



    /// <summary>
    /// Function used to end the level.
    /// </summary>
    public void TheEnd()
    {
        finished = true;
        player.rb.velocity = new Vector3(0, player.rb.velocity.y, 0);
        player.enabled = false;
                
        if (isTimer && timeLeft <= 0)
        {
            endText.text = "End of time! Your score is " + score + " points.";
            audioSource.clip = audioClip[0];
            audioSource.Play();
        }
        else
        {
            endText.text = "Level completed! Your score is " + score + " points.";
            audioSource.clip = audioClip[1];
            audioSource.Play();
        }

        SetHighScore(score);
    }


    /// <summary>
    /// Save best score for given level.
    /// </summary>
    /// <param name="newHighScore">Score you want to save.</param>
    private void SetHighScore(int newHighScore)
    {
        int oldHighScore = PlayerPrefs.GetInt("Highscore " + LevelManager.Instance.GetCurrentSceneName(), 0);

        if (newHighScore > oldHighScore)
        {
            PlayerPrefs.SetInt("Highscore " + LevelManager.Instance.GetCurrentSceneName(), newHighScore);
        }

        UpdateHighScore();
    }


    /// <summary>
    /// Update GUI high score text
    /// </summary>
    private void UpdateHighScore()
    {
        highScoreText.text = "HighScore: " + PlayerPrefs.GetInt("Highscore " + LevelManager.Instance.GetCurrentSceneName(), 0).ToString();
    }
}