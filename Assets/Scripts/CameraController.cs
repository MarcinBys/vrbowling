﻿using UnityEngine;
using System.Collections;
using System;

public class CameraController : MonoBehaviour
{
    public GameObject player;
    private Vector3 offset;

	float xAxis;
	float yAxis;
 
	public float distance = 10.0f;
	public float speed = 5.0f;

    bool vrOn;


    private void Start()
    {
        vrOn = Convert.ToBoolean(PlayerPrefs.GetInt("VR_On"));
    }


    void LateUpdate()
    {
        if (vrOn)
        {
            CameraVR();
        }
        else if (!vrOn)
        {
            orbitFollowCamera();
        }
    }


    void orbitFollowCamera()
    {
        

        xAxis += Input.GetAxis("Mouse X") * speed;
        yAxis -= Input.GetAxis("Mouse Y") * speed;
      

        if (yAxis <= 10)
        {
            yAxis = 10;

        }
        else if (yAxis >= 50)
        {
            yAxis = 50;
        }

        Quaternion rotation = Quaternion.Euler(yAxis, xAxis, 0.0f);

        transform.rotation = rotation;

        transform.position = player.transform.position + rotation * new Vector3(0.0f, 0.0f, -distance);
    }


    private void CameraVR()
    {
        xAxis += Input.GetAxis("Horizontal");
        yAxis -= Input.GetAxis("Mouse Y") * speed;

        if (yAxis <= 10)
        {
            yAxis = 10;

        }
        else if (yAxis >= 50)
        {
            yAxis = 50;
        }

        Quaternion rotation = Quaternion.Euler(yAxis, xAxis, 0.0f);

        transform.rotation = rotation;

        transform.position = player.transform.position + rotation * new Vector3(0.0f, 0.0f, -distance);
      
    }
}