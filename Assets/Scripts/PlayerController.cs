﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour {


    // <<< PHYSICS >>>
    [HideInInspector] public Rigidbody rb;
    [SerializeField] private float speed;
    private float defaultSpeed;

    // <<< MOVEMENT >>>
    Quaternion player_rotation;
    float rotation_value;

    // <<< VR MOVEMENT >>>
    float moveVerticalVR;
    float moveHorizontalVR;
    bool vrOn;

    // <<< JUMPING >>>
    public bool grounded;
    [Range(0,10)] public int jumpVelocity;
    [SerializeField] private float fallingMultiply = 2.5f;      //2.25 for normal gravity levels
    [SerializeField] private float softJumpMultiply = 2f;      //1 for normal gravity levels

    // <<< BUTTONS NAMES >>>
    private string jumpButton = "Jump";

    // << AUDIO >>>
    private AudioSource audioSource;
    public AudioClip[] audioClips;


	void Start () {
        audioSource = GetComponent<AudioSource>();

        rb = GetComponent<Rigidbody>();
        defaultSpeed = speed;

        vrOn = Convert.ToBoolean(PlayerPrefs.GetInt("VR_On"));
    }
	

    // Update is called once per frame
	void Update () {
        GravityEnhancement();

        if (Input.GetButtonDown(jumpButton) && grounded)
        {
            Jump();
        }
    }



    void FixedUpdate()
    {
        if (vrOn)
        {
            MovementVR();
        }
        else if (!vrOn)
        {
            Movement();
        }
    }


    /// <summary>
    /// Handle player movement.
    /// </summary>
    private void Movement()
    {
        float moveVertical = Input.GetAxis("Vertical");
        float moveHorizontal = Input.GetAxis("Horizontal");
        
		Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        
        rotation_value = Camera.main.transform.eulerAngles.y;
        player_rotation = Quaternion.Euler(0.0f, rotation_value, 0.0f);
        movement = player_rotation * movement;
        rb.AddForce(movement * speed);
    }
    
    private void MovementVR()
    {
        moveVerticalVR = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(0.0f, 0.0f, moveVerticalVR);

        if (Input.GetKey("l")) movement += Vector3.left;
        if (Input.GetKey("r")) movement += Vector3.right;

        rotation_value = Camera.main.transform.eulerAngles.y;
        player_rotation = Quaternion.Euler(0.0f, rotation_value, 0.0f);
        movement = player_rotation * movement;
        rb.AddForce(movement * speed);
    }
    
    /// <summary>
    /// Handle player jumping.
    /// </summary>
    private void Jump()
    {
        Vector3 velo = rb.velocity;
        velo.y = jumpVelocity;
        
        rb.velocity = velo;
        grounded = false;

        audioSource.clip = audioClips[0];
        audioSource.volume = 1f;

        audioSource.Play();
    }


    /// <summary>
    /// Enhance jump gravity (faster falling down than getting up)
    /// </summary>
    private void GravityEnhancement()
    {
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector3.up * Physics.gravity.y * Time.deltaTime * fallingMultiply;
        }
        else if (rb.velocity.y > 0 && !Input.GetButton(jumpButton))
        {
            rb.velocity += Vector3.up * Physics.gravity.y * Time.deltaTime * softJumpMultiply;
        }
    }



    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))  // detect if the player is on the ground
        {
            grounded = true;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (grounded && !audioSource.isPlaying && rb.velocity.magnitude >= 1f && collision.gameObject.CompareTag("Ground"))
        {
            audioSource.clip = audioClips[1];
            audioSource.volume = 0.7f;

            audioSource.Play();
        }
        else if (grounded && audioSource.isPlaying && rb.velocity.magnitude < 1f && collision.gameObject.CompareTag("Ground"))
        {
            audioSource.Pause();
        }
    }



    /// <summary>
    /// Function adding movement speed to player.
    /// </summary>
    /// <param name="value">Speed value to add to the actual movement speed.</param>
    public void AddSpeed(float value)
    {
        speed += value;
    }
}