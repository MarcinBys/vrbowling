﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour {

    // <<< SCRIPT ACCESS >>>
    GameController gameController;
    PlayerController player;

    public AudioClip[] audioClip;
    private AudioSource audioSource;

    // <<< TYPE OF OBJECT >>>
    [Tooltip("Select if object is a bowling pin.")]
    public bool bowlingPin;
    private bool fallen = false;

    [Tooltip("Select if object is used to speed up a player.")]
    public bool speedBooster;

    [Tooltip("Select if object is used to increase time left.")]
    public bool timeBooster;

    [Tooltip("Select if object is used to finish the level.")]
    public bool finish;


    // <<< OBJECT AWARDS >>>
    [Tooltip("Points awarded for this object.")]
    public int points;

    [Tooltip("Time awarded for this object (if TimeBooster == TRUE)")]
    public int timeToAdd;


    // <<< OBJECT BEHAVIOUR >>>
    [Tooltip("Rotate an object 360 degrees")]
    public bool rotate;
    public int rotateSpeed;



    void Start () {
        audioSource = GetComponent<AudioSource>();

        gameController = FindObjectOfType<GameController>();

        if (speedBooster)
        {
            player = FindObjectOfType<PlayerController>();
        }
    }


    private void FixedUpdate()
    {
        if (rotate)
        {
            //gameObject.transform.Rotate(Vector3.right * Time.deltaTime * 10);
            gameObject.transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);        // rotate 360 degrees
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        // Check if BowlingPin is hit by Player or BowlingPin (combo) and is not already fallen
        if (bowlingPin && (collision.gameObject.CompareTag("BowlingPin") || collision.gameObject.CompareTag("Player")) && !fallen) //!collision.gameObject.transform.rotation.Equals(90))
        {
            gameController.AddScore(points);
            fallen = true;

            float pitch = Random.Range(0.5f, 2.5f);

            audioSource.pitch = pitch;
            audioSource.clip = audioClip[0];
            audioSource.Play();
        }

    }



    private void OnTriggerEnter(Collider other)
    {
   
        if (finish && other.CompareTag("Player"))
        {
            gameController.TheEnd();
            gameController.AddScore(points);
        }
        else if (speedBooster && other.CompareTag("Player"))
        {
            player.AddSpeed(30);
            gameController.AddScore(points);
        }
        else if (timeBooster && other.CompareTag("Player"))
        {
            gameController.AddTime(timeToAdd);
            gameController.AddScore(points);
            gameObject.SetActive(false);
        }
    }
}
